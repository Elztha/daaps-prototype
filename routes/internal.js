const express = require('express');
const router = express.Router();
// Load User model
const User = require('../models/User');
const { ensureAuthenticated, forwardAuthenticated } = require('../config/auth');

router.get('/trade',ensureAuthenticated, (req, res) => {});
router.get('/buy',ensureAuthenticated, (req, res) => {});

router.post('/trade',ensureAuthenticated, (req, res) => {
  let user = req.user;
  const {authenticate,
         sell_currency,
         sell_price,
         buy_currency,
         buy_price,
         min_buy_amount,
         sell_method,
         deposit_amount} = req.body;

  let errors = [];
  var lewat = 0;
  if (authenticate != 'aaaaaa') {
    errors.push({ msg: 'Authentication Failed' });
  }
  function insertTrade(){
  if(errors.length > 0){
            console.log("Errors found redirecting to sell...")
            console.log(errors);
            res.render('sell', { errors });
        }
        else{
            console.log("You can afford this trade!");
            User.findOneAndUpdate(
            { email : req.user.email ,  },  // search query
            { $push: { "trade" : { 
               sell_currency : sell_currency,
               sell_price : sell_price,
               buy_currency : buy_currency,
               buy_price : buy_price,
               min_buy_amount : min_buy_amount,
               sell_method : sell_method,
               deposit_amount : deposit_amount, }
              }
            }, 
            { runValidators: true ,overwrite: true   })        // validate before update
            .then(console.log("You are selling " + sell_price +" "+ sell_currency + " for "+ buy_price +" "+ buy_currency ),res.redirect('/dashboard'))
            .catch(err => {console.error(err)})
        }
}
  if (!sell_currency || !sell_price || !buy_currency || !buy_price || !min_buy_amount || !sell_method) {
    errors.push({ msg: 'Please enter all fields' });
    console.log(errors);
    res.render('sell', {
      errors,
    });
  }

  if(sell_method == 'autocharge'){
    console.log("Autocharged RUN")
    User.find({ email: req.user.email },(err,data) =>{
    for(var b = 0; b < data[0].wallet.length; b++){  
      console.log("checking wallet " +data[0].wallet[b].currency +" "+ data[0].wallet[b].amount);
      console.log(data[0].wallet[b].currency == sell_currency && data[0].wallet[b].amount < min_buy_amount)
        if(data[0].wallet[b].currency == sell_currency && data[0].wallet[b].amount < min_buy_amount){
        errors.push({ msg: 'Not Sufficient Amount in Wallet to be Autocharged' });
        }else{};
    }insertTrade();
  })
  }
  else{ // sell_method = 'deposit'
    console.log("Deposit RUN")
    if(deposit_amount < min_buy_amount){errors.push({ msg: 'Not Sufficient Amount of Deposit' });}
    User.find({ email: req.user.email },(err,data) =>{
    for(var b = 0; b < data[0].wallet.length; b++){  
      console.log("checking wallet " +data[0].wallet[b].currency +" "+ data[0].wallet[b].amount);
      console.log(data[0].wallet[b].currency == sell_currency && data[0].wallet[b].amount < deposit_amount)
        if(data[0].wallet[b].currency == sell_currency && data[0].wallet[b].amount < deposit_amount){
        errors.push({ msg: 'Not Sufficient Amount in Wallet to be Deposited' });
        }
        else if(data[0].wallet[b].currency == sell_currency && data[0].wallet[b].amount >= deposit_amount){
           User.updateOne(
             { email : req.user.email , "wallet.currency" : sell_currency  },  // search query
             { $inc: {"wallet.$.amount" : - deposit_amount }}, 
              {
                runValidators: true ,
                overwrite: true 
              },(err,data) =>{
              }
           ) 
        }
     }insertTrade()
    })
  }
    
});;


        

router.post('/topup', (req, res) => {
  const { new_currency,new_amount } = req.body;
  let errors = [];
  let user = req.user;
  if (!new_amount || !new_currency) {
    errors.push({ msg: 'Please enter all fields' });
  }

  if (new_currency.length > 3) {
    errors.push({ msg: 'Invalid Input of Currency' });
  }

  User.find({ email: req.user.email },(err,data) =>{
    var z = 0;
    for(var b = 0; b < data[0].wallet.length; b++){  
      console.log("checking wallet " +data[0].wallet[b].currency +" "+ data[0].wallet[b].amount);
      console.log(data[0].wallet[b].currency == new_currency)
        if(data[0].wallet[b].currency == new_currency){
        z++;
        User.updateOne(
             { email : req.user.email , "wallet.currency" : new_currency  },  // search query
             { $inc: {"wallet.$.amount" : + new_amount },
               $set: {"wallet.$.date" : Date.now()}
              }, 
              {
                runValidators: true ,
                overwrite: true 
              },(err,data) =>{
                //console.log(data)
                console.log("TOP UP INCREMENT SUCCESS")      
              }
           ) 
        }
    }if(errors.length <= 0 && z == 0){
            User.findOneAndUpdate(
              { email : user.email },  // search query
              { $push: { "wallet" : { currency: new_currency, amount: new_amount } } } , 
              { new: true,                       // return updated doc
                runValidators: true              // validate before update
            })
            .then(console.log("You Received " + new_amount +" "+ new_currency))
            .catch(err => {
              console.error(err)
            })  
            }
  })
  res.redirect('/dashboard');
});

router.post('/search', ensureAuthenticated,(req,res) =>{
  const { buy_currency_search } = req.body;
  let query = buy_currency_search.toUpperCase()
  let user = req.user;
  if(buy_currency_search == ''){
  User.find({},(err,data) =>{
    res.render('buy', {
      go : data,
      user : req.user
    })
  })}
  else{
    User.find({ 
    'trade.buy_currency' : query
  },(err,data) =>{
    console.log(query);
    for (var a = 0; a < data.length; a++){
      /*var vi = (data[a].trade.filter((x) => x.buy_currency == buy_currency_search));
      console.log(data[a].trade.filter((x) => x.buy_currency == buy_currency_search));*/
      for (var b = 0; b < data[a].trade.length; b++){  
          if(data[a].trade[b].buy_currency != query){
            console.log(data[a].trade[b]);
            data[a].trade.splice(b,1);
            b--
          };
    }}
    res.render('buy', {
      go : data,
      user : req.user
    })
  });
  }
});

router.post('/buy', ensureAuthenticated,(req,res) =>{
 let user = req.user;
 const {buyer_amount,id_transaksi} = req.body;
 console.log("ini buyer_amount : "+ buyer_amount);

 User.find({ 'trade._id' : id_transaksi },(err,data) =>{
    console.log("ini ID : "+ id_transaksi);
    //console.log(data);

      /*var vi = (data[a].trade.filter((x) => x.buy_currency == buy_currency_search));
      console.log(data[a].trade.filter((x) => x.buy_currency == buy_currency_search));*/
      for (var b = 0; b < data[0].trade.length; b++){  
          if(data[0].trade[b]._id == id_transaksi){
            console.log(data[0].trade[b]);
            let incremen = data[0].trade[b].sell_amount / data[0].trade[b].buy_amount * buyer_amount;
          };
       }
       User.findOneAndUpdate(
        { 'trade._id' : id_transaksi },  // search query
        { $inc: { "trade" : { sell_amount: sell_amount, buy_amount: buy_amount } } } , 
        { new: true,                       // return updated doc
          runValidators: true              // validate before update
      })
  })});

router.post('/stop', ensureAuthenticated,(req,res) =>{
  let user = req.user;
  const {id_transaksi} = req.body;
  console.log("STOP DENGAN ENTROSTOP" + user.name);
  console.log("ID :" + id_transaksi);
  User.findOneAndUpdate({ email : req.user.email ,"trade._id" : id_transaksi},
        {$pull : {trade: { _id : id_transaksi} }}, 
        function(err, result){
        if(err){
            console.log(err);
        }
        console.log("RESULT: " + result);
        res.redirect('/dashboard');
    })
});



module.exports = router;
