const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  },
  device_id: {
    type: String,
  },
  wallet: [
  {currency: String ,
   amount: Number ,
   date: {
    type: Date,
    default: Date.now
    },
  }
  ],
  trade: [
  {sell_currency: String,
   sell_price: Number,
   buy_currency: String ,
   buy_price: Number,
   min_buy_amount: Number,
   sell_method:String,
   deposit_amount:Number,
   Isu_date: {
    type: Date,
    default: Date.now
    },
  }
  ]

});

const User = mongoose.model('User', UserSchema);

module.exports = User;
